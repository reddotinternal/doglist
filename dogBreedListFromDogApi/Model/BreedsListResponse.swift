//
//  BreedsListResponse.swift
//  dogBreedListFromDogApi
//
//  Created by Taslima Roya on 6/14/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import Foundation
struct BreedsListResponse: Codable {
    let status: String
    let message: [String: [String]]
}
